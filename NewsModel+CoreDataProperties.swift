//
//  NewsModel+CoreDataProperties.swift
//  NewsApp
//
//  Created by Vasily Agafonov on 24.09.2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//
//

import Foundation
import CoreData


extension NewsModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<NewsModel> {
        return NSFetchRequest<NewsModel>(entityName: "NewsModel")
    }

    @NSManaged public var payload: String

}
