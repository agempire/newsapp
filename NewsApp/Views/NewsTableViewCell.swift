//
//  NewsTableViewCell.swift
//  NewsApp
//
//  Created by Vasily Agafonov on 24.09.2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    static let reuseIdentifier = "NewsTableViewCell"
    
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var newsWatchCounterLabel: UILabel!
}
