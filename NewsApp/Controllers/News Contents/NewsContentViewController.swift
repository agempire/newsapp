//
//  NewsContentViewController.swift
//  NewsApp
//
//  Created by Vasily Agafonov on 24.09.2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import UIKit
import WebKit.WKWebView
import WebKit.WKNavigationDelegate

// TODO: segregate
protocol NewsContentPresenterUIDelegate: AnyObject {
    func newsContentShouldShowLoadingIndicator(_ presenter: NewsContentPresenter)
    func newsContentShouldHideLoadingIndicator(_ presenter: NewsContentPresenter)
    
    func newsContent(_ presenter: NewsContentPresenter, didLoad content: String)
    func newsContent(_ presenter: NewsContentPresenter, didFail error: String)
}

final class NewsContentPresenter {
    
    private let newsId: String
    private let contentLoader: NewsContentLoader
    
    unowned let uiDelegate: NewsContentPresenterUIDelegate
    
    init(id: String, delegate: NewsContentPresenterUIDelegate, loader: NewsContentLoader = TinkoffNewsContentLoader()) {
        newsId = id
        uiDelegate = delegate
        contentLoader = loader
    }
    
    func loadContent() {
        uiDelegate.newsContentShouldShowLoadingIndicator(self)
        
        contentLoader.fetchNewsContent(for: newsId) { (content, error) in
            if let content = content {
                self.handle(content: content)
            }
            
            if let error = error {
                self.handle(error: error)
            }
        }
    }
    
    func webViewDidLoadHtmlString() {
        uiDelegate.newsContentShouldHideLoadingIndicator(self)
    }
    
    private func handle(error: LoadError) {
        DispatchQueue.main.async {
            self.uiDelegate.newsContentShouldHideLoadingIndicator(self)
            
            self.uiDelegate.newsContent(self, didFail: error.localizedDescription)
        }
    }
    
    private func handle(content: TinkoffNewsContentLoader.NewsContent) {
        DispatchQueue.main.async {
            self.uiDelegate.newsContent(self, didLoad: content.payload.content)
        }
    }
}

class NewsContentViewController: UIViewController {
    
    static let storyboardIdentifier = "NewsContentController"
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var presenter: NewsContentPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.navigationDelegate = self
        
        presenter.loadContent()
    }
}

extension NewsContentViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        presenter.webViewDidLoadHtmlString()
    }
}

extension NewsContentViewController: NewsContentPresenterUIDelegate {
    
    func newsContentShouldShowLoadingIndicator(_ presenter: NewsContentPresenter) {
        activityIndicator.isHidden = false
    }
    
    func newsContentShouldHideLoadingIndicator(_ presenter: NewsContentPresenter) {
        activityIndicator.isHidden = true
    }
    
    func newsContent(_ presenter: NewsContentPresenter, didLoad content: String) {
        webView.loadHTMLString(content, baseURL: nil)
    }
    
    func newsContent(_ presenter: NewsContentPresenter, didFail error: String) {
        let alert = UIAlertController(title: "Fetch News Content Failed", message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Got it!", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
