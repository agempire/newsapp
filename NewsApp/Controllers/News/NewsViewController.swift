//
//  NewsViewController.swift
//  NewsApp
//
//  Created by Vasily Agafonov on 24.09.2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    lazy var presenter = NewsListPresenter(delegate: self, router: TinkoffNewsRouter(context: self))
    
    lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(onPullToRefresh), for: .valueChanged)
        return control
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.refreshControl = refreshControl
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillTerminate), name: .UIApplicationWillTerminate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground), name: .UIApplicationDidEnterBackground, object: nil)
        
        presenter.loadNews()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc
    func onPullToRefresh() {
        presenter.refreshNews()
    }
    
    @objc
    func applicationWillTerminate() {
        presenter.persistData()
    }
    
    @objc
    func applicationDidEnterBackground() {
        presenter.persistData()
    }
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NewsTableViewCell.reuseIdentifier, for: indexPath) as? NewsTableViewCell else { fatalError() }
        
        setup(cell: cell, at: indexPath)
        
        return cell
    }
    
    private func setup(cell: NewsTableViewCell, at indexPath: IndexPath) {
        cell.newsTitleLabel.text = presenter.newsTitle(at: indexPath)
        cell.newsWatchCounterLabel.text = "\(presenter.newsWatchCounter(at: indexPath))"
    }
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        presenter.didSelectNews(at: indexPath)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        loadDataIfNeeded()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        guard !decelerate else { return }
        
        loadDataIfNeeded()
    }
    
    private func loadDataIfNeeded() {
        guard let lastVisibleIndexPath = tableView.lastVisibleCellIndexPath else { return }
        
        presenter.viewDidShowNews(at: lastVisibleIndexPath)
    }
}

extension ViewController: NewsListPresenterUIDelegate {
    
    func newsList(_ presenter: NewsListPresenter, didUpdateTable updates: [UpdateType], onComplete: @escaping () -> Void) {
        tableView.performBatchUpdates({
            updates.forEach({ (update) in
                update.apply(to: tableView)
            })
        }) { (_) in
            onComplete()
        }
    }
    
    func newsList(_ presenter: NewsListPresenter, didFail error: String) {
        let alert = UIAlertController(title: "Fetch News Failed", message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Got it!", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func newsListShouldHideRefreshControl(_ presenter: NewsListPresenter) {
        tableView.refreshControl?.endRefreshing()
    }
    
    func newsListShouldHideFooterLoader(_ presenter: NewsListPresenter) {
        tableView.tableFooterView?.isHidden = true
    }
    
    func newsListShouldShowFooterLoader(_ presenter: NewsListPresenter) {
        tableView.tableFooterView?.isHidden = false
    }
}

extension UITableView {
    
    fileprivate var lastVisibleCellIndexPath: IndexPath? {
        return indexPathsForVisibleRows?.max()
    }
}
