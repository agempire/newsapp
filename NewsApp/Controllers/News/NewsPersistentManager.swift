//
//  NewsPersistentManager.swift
//  NewsApp
//
//  Created by Vasily Agafonov on 24.09.2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import Foundation
import CoreData

protocol NewsListPersistentManager {
    associatedtype News
    
    func getNews(onComplete: @escaping ([News]) -> Swift.Void)
    func set(news: [News])
    
    func save()
}

final class CoreDataNewsListPersistentManager: NewsListPersistentManager {
    
    struct News {
        let payload: String
    }
    
    private var isLoadingPersistingStore: Bool = false
    
    private var persistentContainer: NSPersistentContainer?
    
    func getNews(onComplete: @escaping ([CoreDataNewsListPersistentManager.News]) -> Void) {
        do {
            try loadPersistentContainerIfNeeded { (container) in
                container.performBackgroundTask { (context) in
                    do {
                        let news = (try context.fetch(NewsModel.request))
                            .map({ News(payload: $0.payload) })
                        
                        DispatchQueue.main.async {
                            onComplete(news)
                        }
                    } catch {
                        DispatchQueue.main.async {
                            onComplete([])
                        }
                    }
                }
            }
        } catch {
            DispatchQueue.main.async {
                onComplete([])
            }
        }
    }
    
    func set(news: [CoreDataNewsListPersistentManager.News]) {
        do {
            try loadPersistentContainerIfNeeded { (container) in
                container.performBackgroundTask { (context) in
                    do {
                        try context.execute(NewsModel.deleteRequest)
                        
                        news.forEach { (new) in
                            guard let entity = NSEntityDescription.entity(forEntityName: "NewsModel", in: context) else { fatalError("No entity") }
                            
                            let model = NewsModel(entity: entity, insertInto: context)
                            model.payload = new.payload
                        }
                        
                        try context.save()
                    } catch {
                        
                    }
                }
            }
        } catch {
            
        }
    }
    
    func save() {
        do {
            try loadPersistentContainerIfNeeded { (container) in
                do {
                    try container.viewContext.save()
                } catch {
                    
                }
            }
        } catch {
            
        }
    }
    
    private func loadPersistentContainerIfNeeded(onComplete: @escaping (NSPersistentContainer) -> Swift.Void) throws {
        guard !isLoadingPersistingStore else { throw NSError(domain: "CoreDataNewsListPersistentManager", code: 5555, userInfo: ["localizedDescription": "Core Data is already loading"]) }
        
        guard let persistentContainer = persistentContainer else {
            isLoadingPersistingStore = true
            
            let container = NSPersistentContainer(name: "NewsApp")
            container.loadPersistentStores(completionHandler: { (storeDescription, error) in
                self.persistentContainer = container
                self.isLoadingPersistingStore = false
                onComplete(container)
            })
            return
        }
        
        onComplete(persistentContainer)
    }
}
