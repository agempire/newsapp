//
//  NewsPresenter.swift
//  NewsApp
//
//  Created by Vasily Agafonov on 24.09.2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import Foundation
import UIKit.UITableView

enum UpdateType {
    case insert([IndexPath])
    case remove([IndexPath])
    case reload([IndexPath])
    
    func apply(to tableView: UITableView) {
        switch self {
        case .insert(let indexPaths):
            tableView.insertRows(at: indexPaths, with: .automatic)
        case .remove(let indexPaths):
            tableView.deleteRows(at: indexPaths, with: .automatic)
        case .reload(let indexPaths):
            tableView.reloadRows(at: indexPaths, with: .automatic)
        }
    }
}

// TODO: segregate
protocol NewsListPresenterUIDelegate: AnyObject {
    func newsList(_ presenter: NewsListPresenter, didUpdateTable updates: [UpdateType], onComplete: @escaping () -> Swift.Void)
    func newsList(_ presenter: NewsListPresenter, didFail error: String)
    
    func newsListShouldHideRefreshControl(_ presenter: NewsListPresenter)
    
    func newsListShouldHideFooterLoader(_ presenter: NewsListPresenter)
    func newsListShouldShowFooterLoader(_ presenter: NewsListPresenter)
}

class NewsListPresenter {
    
    class News: Codable {
        
        typealias UnderlyingNews = TinkoffNewsLoader.News.Payload
        
        let underlyingNews: UnderlyingNews
        var watchCounter: Int
        
        init(news: UnderlyingNews, counter: Int = 0) {
            underlyingNews = news
            watchCounter = counter
        }
    }
    
    private let newsLoader: NewsLoader
    private let newsRouter: NewsRouter
    
    private let persistentManager = CoreDataNewsListPersistentManager()
    
    private var nextPageIndex: Int = 0
    private var isFetchingData: Bool = false
    
    private var news: [News] = []
    
    unowned let uiDelegate: NewsListPresenterUIDelegate
    
    init(loader: NewsLoader = TinkoffNewsLoader(), delegate: NewsListPresenterUIDelegate, router: NewsRouter) {
        newsLoader = loader
        newsRouter = router
        uiDelegate = delegate
    }
}

extension NewsListPresenter {
    
    var numberOfRows: Int {
        return news.count
    }
    
    func model(at indexPath: IndexPath) -> News {
        return news[indexPath.row]
    }
    
    func newsTitle(at indexPath: IndexPath) -> String {
        return model(at: indexPath).underlyingNews.text
    }
    
    func newsWatchCounter(at indexPath: IndexPath) -> Int {
        return model(at: indexPath).watchCounter
    }
}

extension NewsListPresenter {
    
    func persistData() {
        let encoder = JSONEncoder()
        
        let storeNews = news.compactMap { (new) -> CoreDataNewsListPersistentManager.News? in
            guard let data = try? encoder.encode(new), let string = String(data: data, encoding: .utf8) else { return nil }
            
            return CoreDataNewsListPersistentManager.News(payload: string)
        }
        
        persistentManager.set(news: storeNews)
        persistentManager.save()
    }
    
    func loadNews() {
        persistentManager.getNews { (news) in
            let decoder = JSONDecoder()
            let persistedNews = news.compactMap { (new) -> News? in
                guard let data = new.payload.data(using: .utf8), let new = try? decoder.decode(NewsListPresenter.News.self, from: data) else { return nil }
                
                return new
            }
            
            self.news = persistedNews.sorted(by: { (new1, new2) -> Bool in
                new1.underlyingNews.publicationDate.milliseconds > new2.underlyingNews.publicationDate.milliseconds
            })
            
            let updates = (0..<persistedNews.count)
                .map({ IndexPath(row: $0, section: 0) })
            
            self.uiDelegate.newsList(self, didUpdateTable: [.insert(updates)]) {
                self.fetchNews()
            }
        }
    }
    
    func refreshNews() {
        uiDelegate.newsListShouldHideFooterLoader(self)
        
        let updates = (0..<news.count).map({ IndexPath(row: $0, section: 0) })
        
        news.removeAll()
        nextPageIndex = 0
        isFetchingData = true
        
        uiDelegate.newsList(self, didUpdateTable: [.remove(updates)]) {
            self.fetchNextPageNews(
                onSuccess: { (news) in self.handle(news: news, shouldHideRefreshControl: true) },
                onFail: { (error) in self.handle(error: error) }
            )
        }
    }
    
    func viewDidShowNews(at indexPath: IndexPath) {
        guard shouldLoad(after: indexPath) else { return }
        
        fetchNews()
    }
    
    func didSelectNews(at indexPath: IndexPath) {
        news[indexPath.row].watchCounter = news[indexPath.row].watchCounter + 1
        
        newsRouter.showNewsContent(for: news[indexPath.row].underlyingNews.id)
        
        uiDelegate.newsList(self, didUpdateTable: [.reload([indexPath])]) { }
    }
    
    private func fetchNews() {
        guard !isFetchingData else { return }
        
        isFetchingData = true
        
        fetchNextPageNews(
            onSuccess: { (news) in self.handle(news: news) },
            onFail: { (error) in self.handle(error: error) }
        )
    }
    
    private func shouldLoad(after indexPath: IndexPath) -> Bool {
        return indexPath.row >= news.count - 10
    }
    
    private func handle(error: LoadError) {
        DispatchQueue.main.async {
            self.uiDelegate.newsList(self, didFail: error.localizedDescription)
        }
    }
    
    private func handle(news: TinkoffNewsLoader.News, shouldHideRefreshControl: Bool = false) {
        let newbies = news.payload.filter { (payload) -> Bool in
            !self.news.contains(where: { (new) -> Bool in new.underlyingNews.id == payload.id })
        }
        
        let existingNewsCount = self.news.count
        let newsCount = newbies.count
        
        let indexPaths = (existingNewsCount..<existingNewsCount+newsCount)
            .map({ IndexPath(row: $0, section: 0) })
        
        self.news.append(contentsOf: newbies.map({ News(news: $0) }))
        self.news.sort { (new1, new2) -> Bool in
            new1.underlyingNews.publicationDate.milliseconds > new2.underlyingNews.publicationDate.milliseconds
        }
        self.isFetchingData = false
        
        DispatchQueue.main.async {
            if shouldHideRefreshControl {
                self.uiDelegate.newsListShouldHideRefreshControl(self)
            }
            
            self.uiDelegate.newsList(self, didUpdateTable: [.insert(indexPaths)]) {
                self.uiDelegate.newsListShouldShowFooterLoader(self)
            }
        }
    }
    
    private func fetchNextPageNews(onSuccess: @escaping (TinkoffNewsLoader.News) -> Swift.Void, onFail: @escaping (LoadError) -> Swift.Void) {
        newsLoader.fetchNews(for: nextPageIndex) { (news, error) in
            if let news = news {
                self.nextPageIndex = self.nextPageIndex + 1
                
                onSuccess(news)
            }
            
            if let error = error {
                onFail(error)
            }
        }
    }
}
