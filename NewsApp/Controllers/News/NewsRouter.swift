//
//  NewsRouter.swift
//  NewsApp
//
//  Created by Vasily Agafonov on 24.09.2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import Foundation
import UIKit.UIViewController

protocol NewsRouter {
    func showNewsContent(for id: String)
}

final class TinkoffNewsRouter: NewsRouter {
    
    unowned let viewController: UIViewController
    
    init(context: UIViewController) {
        viewController = context
    }
    
    func showNewsContent(for id: String) {
        guard let vc = viewController.storyboard?.instantiateViewController(withIdentifier: NewsContentViewController.storyboardIdentifier) as? NewsContentViewController else { return }
        
        vc.presenter = NewsContentPresenter(id: id, delegate: vc)
        
        viewController.navigationController?.pushViewController(vc, animated: true)
    }
}
