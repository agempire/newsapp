//
//  LoadError.swift
//  NewsApp
//
//  Created by Vasily Agafonov on 24.09.2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import Foundation

enum LoadError {
    case system(underlyingError: Error?)
    case serialization
    case custom(String)
    
    var localizedDescription: String {
        switch self {
        case .system(let underlyingError):
            return underlyingError?.localizedDescription
                ?? "System error occured. Please try again later."
        case .serialization:
            return "Unable to load data from server. Please try again later."
        case .custom(let description):
            return description
        }
    }
}
