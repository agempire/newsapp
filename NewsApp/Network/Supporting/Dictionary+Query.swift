//
//  Dictionary+Query.swift
//  NewsApp
//
//  Created by Vasily Agafonov on 24.09.2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import Foundation

extension Dictionary where Key == String, Value: Hashable {
    
    var asQuery: String {
        return map({ "\($0.key)=\($0.value)" }).joined(separator: "&")
    }
}
