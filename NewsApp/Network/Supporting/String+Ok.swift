//
//  String+Ok.swift
//  NewsApp
//
//  Created by Vasily Agafonov on 24.09.2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import Foundation

extension String {
    
    static var ok: String {
        return "OK"
    }
}
