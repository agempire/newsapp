//
//  NewsContentLoader.swift
//  NewsApp
//
//  Created by Vasily Agafonov on 24.09.2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import Foundation

protocol NewsContentLoader {
    func fetchNewsContent(for id: String, handler: @escaping (TinkoffNewsContentLoader.NewsContent?, LoadError?) -> Swift.Void)
}

final class TinkoffNewsContentLoader: NewsContentLoader {
    
    struct NewsContent: Codable {
        
        struct Payload: Codable {
            
            struct CreationDate: Codable {
                let milliseconds: Double
            }
            
            typealias LastModificationDate = CreationDate
            typealias Title = TinkoffNewsLoader.News.Payload
            
            let title: Title
            let content: String
            let creationDate: CreationDate
            let lastModificationDate: LastModificationDate
        }
        
        let resultCode: String
        let payload: Payload
    }
    
    lazy var session: URLSession = {
        let configuration = URLSessionConfiguration.default
        configuration.waitsForConnectivity = true
        return URLSession(configuration: configuration)
    }()
    
    func fetchNewsContent(for id: String, handler: @escaping (TinkoffNewsContentLoader.NewsContent?, LoadError?) -> Void) {
        session.dataTask(with: .tinkoffNewsContent(for: id)) { (data, _, error) in
            guard let data = data else {
                handler(nil, .system(underlyingError: error))
                return
            }
            
            guard let content = try? JSONDecoder().decode(NewsContent.self, from: data), content.resultCode == .ok else {
                handler(nil, .serialization)
                return
            }
            
            handler(content, nil)
        }.resume()
    }
}

extension URL {
    
    fileprivate static func tinkoffNewsContent(for id: String) -> URL {
        let query = ["id": id].asQuery
        
        guard let url = URL(string: "https://api.tinkoff.ru/v1/news_content?\(query)")
            else { fatalError("Cannot instantinate Tinkoff News Content url") }
        
        return url
    }
}
