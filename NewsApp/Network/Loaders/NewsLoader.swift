//
//  NewsLoader.swift
//  NewsApp
//
//  Created by Vasily Agafonov on 24.09.2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import Foundation

protocol NewsLoader {
    func fetchNews(for page: Int, handler: @escaping (TinkoffNewsLoader.News?, LoadError?) -> Swift.Void)
}

final class TinkoffNewsLoader: NewsLoader {
    
    struct News: Codable {
        
        struct Payload: Codable {
            
            struct PublicationDate: Codable {
                let milliseconds: Double
            }
            
            let id: String
            let text: String
            let publicationDate: PublicationDate
        }
        
        let resultCode: String
        let payload: [Payload]
    }
    
    lazy var session: URLSession = {
        let configuration = URLSessionConfiguration.default
        configuration.waitsForConnectivity = true
        return URLSession(configuration: configuration)
    }()
    
    func fetchNews(for page: Int, handler: @escaping (TinkoffNewsLoader.News?, LoadError?) -> Swift.Void) {
        session.dataTask(with: .tinkoffNews(for: page)) { (data, _, error) in
            guard let data = data else {
                handler(nil, .system(underlyingError: error))
                return
            }
            
            guard let news = try? JSONDecoder().decode(News.self, from: data), news.resultCode == .ok else {
                handler(nil, .serialization)
                return
            }
            
            handler(news, nil)
        }.resume()
    }
}

extension URL {
    
    fileprivate static func tinkoffNews(for page: Int) -> URL {
        let query = [
            "first": page * .newsPerPage,
            "last": (page + 1) * .newsPerPage
        ].asQuery
        
        guard let url = URL(string: "https://api.tinkoff.ru/v1/news?\(query)")
            else { fatalError("Cannot instantinate Tinkoff News url") }
        
        return url
    }
}

extension Int {
    
    fileprivate static var newsPerPage: Int {
        return 20
    }
}
