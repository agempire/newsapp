//
//  NewsModel+CoreDataClass.swift
//  NewsApp
//
//  Created by Vasily Agafonov on 24.09.2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//
//

import Foundation
import CoreData


public class NewsModel: NSManagedObject {

    static var request: NSFetchRequest<NewsModel> {
        return NSFetchRequest<NewsModel>(entityName: "NewsModel")
    }
    
    static var deleteRequest: NSBatchDeleteRequest {
        let delete = NSFetchRequest<NSFetchRequestResult>(entityName: "NewsModel")
        return NSBatchDeleteRequest(fetchRequest: delete)
    }
}
